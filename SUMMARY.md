# Summary

## Part I

* [RCbenchmark store and blog](https://www.rcbenchmark.com/)

## Motor and propeller testing

* [Setup of RCbenchmark tools](motor-and-propeller-testing/setup-of-rcbenchmark-tools.md)
  * [RCbenchmark software](motor-and-propeller-testing/setup-of-rcbenchmark-tools/rcbenchmark-software.md)
  * [Series 1520](motor-and-propeller-testing/setup-of-rcbenchmark-tools/series-1520.md)
  * [Series 1580](motor-and-propeller-testing/setup-of-rcbenchmark-tools/series-1580.md)
  * [Series 1780](/motor-and-propeller-testing/setup-of-rcbenchmark-tools/series-1780.md)
  * [Test](motor-and-propeller-testing/setup-of-rcbenchmark-tools/test.md)
* [Testing](motor-and-propeller-testing/testing.md)
  * [Motor Theory](https://www.google.com/)
  * [Propeller Theory](https://www.rcbenchmark.com/)

## Motion Capture

* [Otus Tracker](otus-tracker.md)
* [Introduction](README.md)

